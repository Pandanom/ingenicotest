﻿// IngenicoTest.cpp : Defines the entry point for the application.
//

#include "IngenicoTest.h"
#include "DataStructures.h"
#include "ProducerConsumer.h"
#include <thread>
#include <vector>

using namespace std;

shared_ptr<MTQueue> q = make_shared<MTQueue>();

int main()
{	
	int numOfClients;
	cout << "Enter number of clients: ";
	cin >> numOfClients;
	vector<std::thread> ct;
	ct.reserve(numOfClients);

	for (int i = 0; i < numOfClients; i++)
	{
		shared_ptr<Client> c = make_shared<Client>(i, q);
		ct.push_back(std::thread(&Client::Run, c));
	}

	shared_ptr<Server> s = make_shared<Server>(q, make_shared<Logger>("log.txt"));

	std::thread server(&Server::Run, s);

	
	server.join();
	for (int i = 0; i < numOfClients; i++)
		ct[i].join();
	
	return 0;
}
