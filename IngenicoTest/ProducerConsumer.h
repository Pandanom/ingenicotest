#pragma once
#include "DataStructures.h"
#include <fstream>


class Logger
{
	std::ofstream logStream;
public:
	Logger(std::string filePath_);
	~Logger();
	void Log(std::string msg_);
};


class Client
{
	int id; 
	std::shared_ptr<MTQueue> queue;

	STDATA GenerateData();

	void SendData(STDATA data_);

public:

	Client(int id_, std::shared_ptr<MTQueue> queue_) : id(id_), queue(queue_){};

	void Run();
};

class Server
{
	std::shared_ptr<MTQueue> queue;
	std::shared_ptr <Logger> logger;

	STDATA ReadMessage();

	void LogMessage(STDATA msg_);

public:

	Server(std::shared_ptr<MTQueue> queue_, std::shared_ptr <Logger> logger_) : queue(queue_), logger(logger_){};

	void Run();

};
