#include "DataStructures.h"
#include <chrono>

#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

void MTQueue::Push(STDATA data_) 
{
	std::unique_lock<std::mutex> ul(g_mutex);
	// there is no need to push message if server is not able to process all messages
	// to prevent memory overflow and possible crashes / data loss we push only if queue has less than 10000 elements
	cv.wait_for(ul, std::chrono::milliseconds(10), [&] {return GetSize() < 10000; }); 
	queue.push(data_);
	ul.unlock();
	cv.notify_one();
}

STDATA MTQueue::Pop()
{
	std::unique_lock<std::mutex> ul(g_mutex, std::defer_lock_t{});
	//as we have only one consumer and many producers
	//cv.wait(ul, [&] {return !Empty(); });
	//yield approach should have faster response time
	while (Empty())
	{
		std::this_thread::yield();
	} 
	ul.lock();
	STDATA ret (queue.top());
	queue.pop();
	ul.unlock();
	//cv.notify_one();
	return ret;
}


inline bool MTQueue::Empty()
{
	std::unique_lock<std::mutex> ul(g_mutex);
	return queue.empty();
}


//predicate in wait_for is locked, but its make those functions unconsistent
//I guess It would be better to use std::this_thread::yield() for both client and server, but theoretically it would be less efficient
// this is the reasone why they are private
inline int MTQueue::GetSize()
{
	return queue.size();
}
