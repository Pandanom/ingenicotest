#pragma once

#include <queue>
#include <windows.h>
#include <mutex>

struct Message
{

};

//I dont like this approach to create struct at all
//but I will because it's seems like it's part of the task. 
//would prefere to use more modern c++ approach
typedef struct tagTDATA {
	BYTE cPriority; //request priority 0 � 255 (0 � the highest priority)
	DWORD dwTicks; //time when request forming in system ticks
	DWORD dwClientId; //unique client identifier
	char Data[255]; //abstract data
} TDATA, *PTDATA;

using STDATA = std::shared_ptr<TDATA>;

//operator for priority_queue sort
inline bool operator <(const STDATA &x, const STDATA &y) {
	return (x->cPriority > y->cPriority);
}

//My thread safe priority_queue wrapper
class MTQueue
{
	//queue for shared ptr od TDATA
	std::priority_queue<STDATA, std::vector<STDATA>> queue;
	//mutex for syncronisation
	std::mutex g_mutex;
	std::condition_variable cv;
	
	bool Empty();

	int GetSize();

public:

	void Push(STDATA data_);

	STDATA Pop();

	//todo: iterator
};