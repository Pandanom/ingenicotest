#include "ProducerConsumer.h"

#include <time.h>
#include <iostream>
#include <string>   
#include <sstream> 
#include <random>

STDATA Client::GenerateData()
{
	//random numbers generation
	std::random_device rd; 
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> distrib(0, 255);

	//simulate data creation process
	std::this_thread::sleep_for(std::chrono::microseconds(1));
	
	STDATA data = std::make_shared<TDATA>();
	data->dwClientId = id;

	//create random priority
	data->cPriority = distrib(gen);

	//generate message
	for (int i = 0; i < 256; i++)
		data->Data[i] = distrib(gen);

	//save creation time
	data->dwTicks = clock();

	return data;
}

void Client::SendData(STDATA data_)
{
	//sends data to priority_queue
	queue->Push(data_);
}

void Client::Run()
{
	STDATA data;
	while (true)
	{
		data = GenerateData();
		SendData(data);
	}
}



STDATA Server::ReadMessage()
{
	return queue->Pop();
}

void Server::LogMessage(STDATA msg_)
{
	//auto ret = msg_
	std::ostringstream ss;
	ss << "Client Id: " << msg_->dwClientId << "| Priority: " << (int)msg_->cPriority << "| Creation time: " << msg_->dwTicks;
	logger->Log(ss.str());
}

void Server::Run()
{
	STDATA msg;
	while (true)
	{
		msg = ReadMessage();
		LogMessage(msg);
	}

}


Logger::Logger(std::string filePath_) 
{
	logStream.open(filePath_);
}

Logger::~Logger() 
{
	logStream.close();
}


void Logger::Log(std::string msg_)
{
	logStream << "Time: (" << clock() <<") |" << msg_ << std::endl;
	std::cout << "Time: (" << clock() << ") |" << msg_ << std::endl;
}